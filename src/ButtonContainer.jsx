import styles from "./App.module.css"

const ButtonContainer = ({onClickButton}) =>{
  let buttonItems = ["C"
  ,'1'
  ,'2'
  ,'+'
  ,'3'
  ,'4'
  ,'-'
  ,'5'
  ,'6'
  ,'*'
  ,'7'
  ,'8'
  ,'/'
  ,'='
  ,'9'
  ,'0'
  ,'.'
  ];
return (
  <>

{buttonItems.map((items)=>(
<button key ={items} className={styles.button} onClick={()=>onClickButton(items)}>{items}</button>
))}

</>
);

}
export default ButtonContainer;
