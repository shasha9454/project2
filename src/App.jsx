import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import styles from "./App.module.css"
import Display from './Display'
import ButtonContainer from './ButtonContainer'



function App() {
  const[calVal , setCaVal] = useState("");
const onClickButton=(items)=>{
if(items === 'C'){
let value = "";
setCaVal(value);
}else if (items === '=') {
let result = eval(calVal);
setCaVal(result);
}else{
  let newItems = calVal + items ;
  console.log("the item is : " + newItems);
  setCaVal(newItems);
}
}
  return (

    <>

<div className={styles.MainContainer}>
<div className={styles.buttonContainer}>
<Display displayValue={calVal}></Display>
<ButtonContainer onClickButton={onClickButton}></ButtonContainer>
</div>
</div>

    </>
  )
}

export default App
